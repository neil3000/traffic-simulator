// Squelch warnings of all imports from the image assets dir
declare module '$lib/assets/*' {
    const image: Record<string, any>[]
    export default image
}