import type Car from '$lib/Car';
import type Road from '$lib/Road';
import type { p5 } from 'p5-svelte';
import { Tile } from './Tile';

const BEND_TIME = 7

export default class Bend extends Tile {
    readonly displayName: string = "Bend"; // Required for the editor's placement UI
    readonly RADIUS: number = 8; // Required to be displayed
    readonly COLOR: { r: number, g: number, b: number } = { r: 189, g: 82, b: 247 }; // Required to be displayed

    car: Car | null = null;
    timer: number = 0;

    constructor(id: number, x: number, y: number) {
        super(id, x, y)
    }

    /**
    * Updates the component logic.
    *
    * @param p5 A reference to the p5 library.
    */
    update(p5: p5): void {
        super.update(p5)
        
        if (this.car == null) {
            var reqRoad = this.getRandomRoadRequestingTransfer()

            if (reqRoad != null && !this.waitingTransfer) {
                this.authorizeRoadForTransfer(reqRoad)
            }
        } else {
            this.timer++
            this.car.update()

            if (this.timer >= BEND_TIME) {
                var road = this.getRandomRoadAvailableForTransfer(this.car)

                if (road != null) {
                    road.transfer(this.car);
                    this.timer = 0;
                    this.car = null;
                }
            }
        }
    }

    /**
    * Draws the component.
    *
    * @param p5 A reference to the p5 library.
    * @param selected A boolean value indicating whether the component is selected.
    */
    draw(p5: p5, selected: boolean): void {
        super.draw(p5, selected)

        if (this.car != null) {
            this.car.draw(p5, this.x, this.y)
        }
    }

    /**
     * Transfer a Car to this Tile and unmarks this Tile as waiting a transfer.
     *
     * @params car The car to be transferred.
     */
    transfer(car: Car): void {
        this.car = car;

        super.transfer(car)
    }
}