import type { p5 } from 'p5-svelte';

const BASE_SPEED = 1
const SPEED_VARIANCE = .07 // +/-

export default class Car {
  speed: number;
  path: number[];
  measuring: boolean;
  aliveTicks: number = 0;

  constructor(path: number[] = [], measuring: boolean = false) {
    this.speed = BASE_SPEED + Math.abs(Math.random() * (2 * SPEED_VARIANCE + 1) - SPEED_VARIANCE);
    this.path = [...path];
    this.measuring = measuring;
  }

  update() {
    this.aliveTicks++;
  }

  draw(p5: p5, x: number, y: number) {
    p5.strokeWeight(0)
    p5.fill(this.measuring?0:72, this.measuring?128:215, this.measuring?128:255)
    p5.circle(x, y, 10)
  }
}