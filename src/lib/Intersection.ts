import type Car from '$lib/Car';
import type { p5 } from 'p5-svelte';
import { Tile } from '$lib/Tile';

const BEND_TIME = 7

export default class Intersection extends Tile {
    readonly displayName: string = "Intersection";
    readonly RADIUS: number = 8;
    readonly COLOR: { r: number, g: number, b: number } = { r: 255, g: 165, b: 0 };

    car: Car | null = null;
    timer: number = 0;
    nextPath: number = 0;

    /**
    * Updates the component logic.
    *
    * @param p5 A reference to the p5 library.
    */
    update(p5: p5) {
        super.update(p5)

        if (this.car == null) {
            var reqRoad = this.getRandomRoadRequestingTransfer()

            if (reqRoad != null && !this.waitingTransfer) {
                this.authorizeRoadForTransfer(reqRoad)
            }
        } else {
            this.timer++
            this.car.update()

            if (this.timer >= BEND_TIME) {
                /*var avRoads = this.getRoadsAvailableForTransfer(this.car).filter((v) => v.nodeId2 == this.car!!.path.pop())

                if (avRoads.length > 0) {
                    var r = this.getRandomFromList(avRoads)

                    if (r != null) {
                        r.transfer(this.car);
                        this.timer = 0;
                        this.car = null;
                    }
                }*/

                var tmpRoads = this.outputRoads.filter((v) => v.nodeId2 == this.nextPath)
                if (tmpRoads.length > 0) {
                    var availableRoads = tmpRoads.filter(r => r.isAcceptingTransfer(this.car!!))

                    if (availableRoads.length > 0) {
                        var r = this.getRandomFromList(availableRoads)
                        if (r != null) r.transfer(this.car)
                        this.car = null
                        this.timer = 0
                    }
                } else {
                    this.car = null
                    this.timer = 0
                }
            }
        }/*

        if (this.car != null) {
            this.carTime++
            if (this.carTime >= BEND_TIME) {
                var nextStepID = this.car.path.pop()

                var tmpRoads = this.outputRoads.filter((v) => v.nodeId2 == nextStepID)
                if (tmpRoads.length > 0) {
                    var availableRoads = tmpRoads.filter(r => r.canSpawn(this.car!!))
          
                    if (availableRoads.length > 0) {
                      availableRoads[Math.floor(Math.random() * availableRoads.length)].spawn(this.car)
                      this.car = null
                      this.carTime = 0
                    }
                } else {
                    this.car = null
                    this.carTime = 0
                }
            }
        }*/
    }

    /**
    * Draws the component.
    *
    * @param p5 A reference to the p5 library.
    * @param selected A boolean value indicating whether the component is selected.
    */
    draw(p5: p5, selected: boolean) {
        super.draw(p5, selected)

        if (this.car != null) {
            this.car.draw(p5, this.x, this.y)
        }
    }

    /**
    * Transfer a Car to this Tile and unmarks this Tile as waiting a transfer.
    *
    * @params car The car to be transferred.
    */
    transfer(car: Car): void {
        this.car = car;
        this.timer = 0;
        this.nextPath = this.car.path.pop()!!

        super.transfer(car)
    }
}