import type { p5 } from 'p5-svelte';
import type Car from "./Car";
import type { Tile } from './Tile';

const SECURITY_DISTANCE = 15

export default class Road {
    nodeId1: number;
    nodeId2: number;
    offset: number;
    cars: Array<{ car: Car, dist: number }>;
    totalDist: number;
    nodeStart: Tile | null = null;
    nodeEnd: Tile | null = null;
    authorizedForTransfer: boolean = false;
    requestingTransfer: boolean = false;

    constructor(nodeId1: number, nodeId2: number, offset: number = 0) {
        this.nodeId1 = nodeId1;
        this.nodeId2 = nodeId2;
        this.cars = Array()
        this.totalDist = 0
        this.offset = offset
    }

    setup(tiles: Tile[]) {
        tiles.forEach(t => {
            if (t.id == this.nodeId1) {
                this.nodeStart = t
                t.registerRoad(this, false)
            } else if (t.id == this.nodeId2) {
                this.nodeEnd = t
                t.registerRoad(this, true)
            }
        });
        this.totalDist = Math.sqrt((this.nodeStart!!.x - this.nodeEnd!!.x) ** 2 + (this.nodeStart!!.y - this.nodeEnd!!.y) ** 2)
    }

    update() {
        var maxDist = this.totalDist
        this.cars.forEach(wrapper => {
            wrapper.car.update()

            wrapper.dist = Math.min(wrapper.dist + wrapper.car.speed, maxDist)

            if (wrapper.dist >= this.totalDist - SECURITY_DISTANCE) {
                this.requestingTransfer = true

                if (this.authorizedForTransfer) {
                    if (wrapper.dist >= this.totalDist) {
                        this.nodeEnd!!.transfer(this.cars.shift()?.car!!)
                        this.authorizedForTransfer = false
                        this.requestingTransfer = false
                    }
                } else {
                    wrapper.dist = this.totalDist - SECURITY_DISTANCE
                }
            }
            maxDist = wrapper.dist - SECURITY_DISTANCE
        });
    }

    draw(p5: p5) {
        p5.stroke(255 * +(this.requestingTransfer&&!this.authorizedForTransfer), 0, 0)
        p5.strokeWeight(1)
        p5.line(this.nodeStart!!.x, this.nodeStart!!.y + this.offset, this.nodeEnd!!.x, this.nodeEnd!!.y + this.offset)
    }

    drawEnd(p5: p5) {
        this.cars.forEach(car => car.car.draw(
            p5,
            this.nodeStart!!.x + car.dist * (this.nodeEnd!!.x - this.nodeStart!!.x) / this.totalDist,
            this.nodeStart!!.y + this.offset + car.dist * (this.nodeEnd!!.y - this.nodeStart!!.y) / this.totalDist
        ));
    }

    isAcceptingTransfer(car: Car) {
        return (this.cars.length + 1) * SECURITY_DISTANCE <= this.totalDist
    }

    transfer(car: Car) {
        this.cars.push({ car: car, dist: 0 })
    }
}