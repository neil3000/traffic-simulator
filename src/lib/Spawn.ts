import type { p5 } from 'p5-svelte';
import { get } from 'svelte/store';

import type Road from '$lib/Road';
import { roads, tiles } from '$lib/store';
import Intersection from '$lib/Intersection';
import { toasts } from '$lib/toasts';
import Car from '$lib/Car';
import { Tile } from '$lib/Tile';

export default class Spawn extends Tile {
    readonly displayName: string = "Spawn";
    readonly RADIUS: number = 15;
    readonly COLOR: { r: number, g: number, b: number } = { r: 140, g: 235, b: 52 };

    target: number | null;
    timer: number = 0;
    computedPath: (number[] | null) = null;
    measuringCount: number = 0;
    spawnRate: number = 40;

    constructor(id: number, x: number, y: number, target: number | null = null) {
        super(id, x, y)
        this.target = target;
    }

    /**
     * Updates the component logic.
     *
     * @param p5 A reference to the p5 library.
     */
    update(p5: p5) {
        if (this.timer >= this.spawnRate) {
            var car = new Car(this.computedPath || Array())
            this.getRoadsAvailableForTransfer(car).forEach(r => {
                r.transfer(new Car(this.computedPath || Array(), this.measuringCount > 0))
                if (this.measuringCount > 0) this.measuringCount--;
            })
            this.timer = 0;
        }
        this.timer++
    }

    /**
     * Computes the shortest path from this Tile to the target Tile.
     */
    computePaths(): void {
        if (this.target != null) {
            let vertices = get(tiles).map(u => u.id)
            let edges: Road[] = get(roads)

            const distances = new Array(vertices.length).fill(Infinity);
            const parents = new Array(vertices.length).fill(null);

            distances[vertices.indexOf(this.id)] = 0;

            for (let i = 0; i < vertices.length - 1; i++) {
                for (const edge of edges) {
                    const u = vertices.indexOf(edge.nodeId1);
                    const v = vertices.indexOf(edge.nodeId2);
                    const weight = edge.totalDist;
                    if (distances[u] + weight < distances[v]) {
                        distances[v] = distances[u] + weight;
                        parents[v] = edge.nodeId1;
                    }
                }
            }

            for (let i = 0; i < vertices.length - 1; i++) {
                for (const edge of edges) {
                    const u = vertices.indexOf(edge.nodeId1);
                    const v = vertices.indexOf(edge.nodeId2);
                    const weight = edge.totalDist;
                    if (distances[u] + weight < distances[v]) {
                        // There is a negative cycle.
                        console.log("RahNeil_N3:Error:NegativeCycle")
                        toasts.error("Negative Cycle found!")
                    }
                }
            }

            const path: number[] = [];
            let currentVertex = this.target;
            while (currentVertex !== this.id) {
                path.unshift(currentVertex);
                currentVertex = parents[vertices.indexOf(currentVertex)];
            }

            var tmpPath = Array()

            for (var i = path.length - 1; i >= 0; i--) {
                if (get(tiles).find(v => v.id == path[i]) instanceof Intersection) tmpPath.push(path[i + 1])
            }

            this.computedPath = tmpPath;
        }
    }
}