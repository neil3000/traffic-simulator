import type { p5 } from 'p5-svelte';
import type Road from './Road';
import type Car from './Car';

export interface TileData {
    readonly displayName: String; // Required for the editor's placement UI
    readonly RADIUS: number; // Required to be displayed
    readonly COLOR: { r: number, g: number, b: number } // Required to be displayed

    readonly id: number;
    readonly x: number;
    readonly y: number;
    outputRoads: Road[];
    inputRoads: Road[];
    waitingTransfer: boolean;
}

export abstract class Tile implements TileData {
    readonly displayName: string = "error"; // Required for the editor's placement UI
    readonly RADIUS: number = 8; // Required to be displayed
    readonly COLOR: { r: number, g: number, b: number } = { r: 255, g: 165, b: 0 }; // Required to be displayed

    readonly id;
    readonly x;
    readonly y;
    outputRoads: Road[] = Array();
    inputRoads: Road[] = Array();
    waitingTransfer: boolean = false;

    constructor(id: number, x: number, y: number) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    /**
    * Returns true if the mouse is currently hovering over the element.
    *
    * @param p5 A reference to the p5 library.
    * @returns A boolean value indicating whether the mouse is currently hovering over the element.
    */
    hovered(p5: p5): boolean {
        return Math.sqrt((p5.mouseX - this.x) ** 2 + (p5.mouseY - this.y) ** 2) <= this.RADIUS
    }

    /**
    * Updates the component logic.
    *
    * @param p5 A reference to the p5 library.
    */
    update(p5: p5): void { }


    /**
    * Draws the component.
    *
    * @param p5 A reference to the p5 library.
    * @param selected A boolean value indicating whether the component is selected.
    */
    draw(p5: p5, selected: boolean): void {
        p5.fill(this.COLOR.r, this.COLOR.g, this.COLOR.b);
        p5.stroke(0)
        p5.strokeWeight(0);
        p5.circle(this.x, this.y, this.RADIUS * 2)

        // Selected & hovered states
        if (selected || this.hovered(p5) && this.id !== -1) {
            p5.noFill()
            p5.strokeWeight(1)
            p5.square(this.x - this.RADIUS - 4, this.y - this.RADIUS - 4, 2 * this.RADIUS + 8, 5)
            if (selected) p5.square(this.x - this.RADIUS - 8, this.y - this.RADIUS - 8, 2 * this.RADIUS + 16, 8)
        }
    }

    /**
    * Registers a road with the system.
    *
    * @param road A Road object representing the road to be registered.
    * @param input Whether the road is an input or output
    */
    registerRoad(road: Road, input: boolean): void {
        if (input) {
            this.inputRoads.push(road)
        } else {
            this.outputRoads.push(road)
        }
    }

    /**
     * Get a random element from a provided list.
     *
     * @param list The list from which to choose a random element.
     * @returns The randomly chosen element from the list, or null if the list is empty.
     */
    getRandomFromList<T>(list: T[] | null): T | null {
        if (list == null || list.length == 0) return null
        return list[Math.floor((Math.random() * list.length))];
    }

    /**
     * Get all roads from this Tile that are available for transfer given a provided Car.
     *
     * @param car The car that would be transferred.
     * @returns The list of roads from this Tile that are available for transfer.
     */
    getRoadsAvailableForTransfer(car: Car): Road[] {
        return this.outputRoads.filter((r: Road) => r.isAcceptingTransfer(car))
    }

    /**
     * Get a random Road from this Tile that is available for transfer given the provided Car.
     *
     * @param car The car that would be transferred.
     * @returns The randomly selected road from this Tile that is available for transfer, or null if no road is available.
     */
    getRandomRoadAvailableForTransfer(car: Car): Road | null {
        return this.getRandomFromList(this.getRoadsAvailableForTransfer(car))
    }

    /**
     * Get all roads from this Tile that are requesting transfer.
     *
     * @returns The list of roads from this Tile that are requesting transfer.
     */
    getRoadsRequestingTransfer(): Road[] {
        return this.inputRoads.filter((r: Road) => r.requestingTransfer)
    }

    /**
     * Get a random Road from this Tile that is requesting transfer.
     *
     * @returns The randomly selected road from this Tile that is requesting transfer, or null if no road is available.
     */
    getRandomRoadRequestingTransfer(): Road | null {
        return this.getRandomFromList(this.getRoadsRequestingTransfer())
    }

    /**
     * Authorize a Road for transfer and mark this Tile as waiting a transfer.
     *
     * @returns The randomly selected road from this Tile that is requesting transfer, or null if no road is available.
     */
    authorizeRoadForTransfer(road: Road): void {
        road.authorizedForTransfer = true
        this.waitingTransfer = true
    }

    /**
     * Transfer a Car to this Tile and unmarks this Tile as waiting a transfer.
     *
     * @params car The car to be transferred.
     */
    transfer(car: Car): void {
        this.waitingTransfer = false
    }

    /**
    * Clears all input and output roads from this Tile.
    * Useful before recomputing paths after a change.
    */
    clearRoads(): void {
        this.inputRoads = Array()
        this.outputRoads = Array()
    }
}