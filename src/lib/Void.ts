import type { p5 } from "p5-svelte";
import { Tile } from "./Tile";
import type Car from "./Car";
import { fast, measuring, measuringLogs, measuringLogsTmp, tiles } from "./store";
import { get } from "svelte/store";
import { toasts } from "./toasts";
import Spawn from "./Spawn";

export default class Void extends Tile {
    readonly displayName: string = "Void"; // Required for the editor's placement UI
    readonly RADIUS: number = 15; // Required to be displayed
    readonly COLOR: { r: number, g: number, b: number } = { r: 245, g: 41, b: 22 }; // Required to be displayed

    constructor(id: number, x: number, y: number) {
        super(id, x, y)
    }

    /**
      * Updates the component logic.
      *
      * @param p5 A reference to the p5 library.
      */
    update(p5: p5): void {
        this.inputRoads.forEach(r => this.authorizeRoadForTransfer(r))
    }

    /**
     * Transfer a Car to this Tile and unmarks this Tile as waiting a transfer.
     *
     * @params car The car to be transferred.
     */
    transfer(car: Car): void {
        if (car.measuring) {
            measuring.update(v => v - 1)
            measuringLogs.update(l => l.concat([car.aliveTicks]))

            if (get(measuring) == 0) {
                /*toasts.success("Done!")
                fast.set(false)
                console.log(get(measuringLogs))*/

                //TEST
                //console.log("=TRANSPOSE(SPLIT(\"" +  + "\", \",\"))")
                measuringLogsTmp.update(v => v.concat([get(measuringLogs)]))

                for (let t of get(tiles)) {
                    if (t instanceof Spawn) {
                        if (t.id == 1) {
                            toasts.success(t.spawnRate.toString())
                        }
                        t.spawnRate--;
                    }
                }

                setTimeout(() => {
                    var spawnCount = 0
                    for (let t of get(tiles)) {
                        if (t instanceof Spawn) {
                            if (t.spawnRate > 19) {
                                t.measuringCount = 200 * t.outputRoads.length
                                spawnCount += t.outputRoads.length
                            }
                        }
                    }
                    if (spawnCount) {
                        measuring.set(spawnCount * 200)
                        measuringLogs.set([])
                    } else {
                        fast.set(false)
                        console.log(get(measuringLogsTmp))

                        var CsvString = "";
                        get(measuringLogsTmp).forEach(function (RowItem, RowIndex) {
                            RowItem.forEach(function (ColItem, ColIndex) {
                                CsvString += ColItem + ',';
                            });
                            CsvString += "\r\n";
                        });
                        CsvString = "data:application/csv," + encodeURIComponent(CsvString);
                        var x = document.createElement("A");
                        x.setAttribute("href", CsvString);
                        x.setAttribute("download", "somedata.csv");
                        document.body.appendChild(x);
                        x.click();
                    }
                }, 15000)
            }
        }
    }
}