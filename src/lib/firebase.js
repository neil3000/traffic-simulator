import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider, OAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import { fetchAndActivate, getRemoteConfig, isSupported, getValue } from 'firebase/remote-config';
import { writable } from 'svelte/store';

import { toasts } from '$lib/toasts';
import { logs } from '$lib/logs';

const firebaseConfig = {
	apiKey: "AIzaSyCphlenpyUOGuzvEZ8CalvhdFDYPdgbW94",
	authDomain: "auth.rahmouni.dev",
	projectId: "rahneil-n3-trafficsimulator",
	storageBucket: "rahneil-n3-trafficsimulator.appspot.com",
	messagingSenderId: "224296014131",
	appId: "1:224296014131:web:ac55f165560cc115f5ee46",
	measurementId: 'G-RPJWSRYEJZ'
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleAuthProvider = new GoogleAuthProvider();
export const microsoftAuthProvider = new OAuthProvider('microsoft.com');
export const appleAuthProvider = new OAuthProvider('apple.com');
export const db = getFirestore(app);

isSupported().then((supported) => {
	if (supported) {
		let rc = getRemoteConfig(app);
		rc.settings.minimumFetchIntervalMillis = 3600000; //ONLY FOR DEV
		fetchAndActivate(rc)
			.then(() => {
				rc_desc_article.update((v) => getValue(rc, 'desc_article').asString() || v);
				rc_neilRahmouni.update((v) => getValue(rc, 'neilRahmouni').asString() ? JSON.parse(getValue(rc, 'neilRahmouni').asString()) : v);
				rc_chadrixy.update((v) => getValue(rc, 'chadrixy').asString() ? JSON.parse(getValue(rc, 'chadrixy').asString()) : v);
				rc_feedback_email.update((v) => getValue(rc, 'feedback_email').asString() || v);
				rc_footer.update((v) => getValue(rc, 'footer') ? JSON.parse(getValue(rc, 'footer').asString()) : v);
				rc_loginProviders.update((v) => getValue(rc, 'loginProviders').asString() ? JSON.parse(getValue(rc, 'loginProviders').asString()) : v);
				rc_banner_project_url.update((v) => getValue(rc, 'banner_project_url').asString() || v);
				
				logs.add({ msg: "Fetched RC values from server" }, "info")
			})
			.catch((err) => {
				console.log(err);
				logs.add(err, "error")
				toasts.feedbackError("31N7BwAzEw@RahNeil_N3:firebase:isSupported:supported:fetchAndActivateRC");
			});
	}
});

export let rc_desc_article = writable("<h1>Optimisation et gestion du trafic routier sous contraintes</h1><h3>Motivation</h3>L'optimisation du trafic routier sous contraintes permet de:<ul><li>Décongestionner les routes</li><li>Réduire la consommation de carburant et des émissions</li><li>Améliorer la sécurité en réduisant le risque d'accidents</li></ul>Dans l'ensemble, l'optimisation et la gestion du trafic routier sous contraintes contribue à rendre nos systèmes de transport plus efficaces, efficients et durables.<br/><br/><a class='btn' href='mcot'>Read the MCOT</a>");

export let rc_neilRahmouni = writable(JSON.parse('{"pfp":null,"main":null,"links":[{"icon":"twitter","url":"http://neil.rahmouni.dev/twitter","description":"Neïl Rahmouni\'s Twitter profile"},{"icon":"instagram","url":"http://neil.rahmouni.dev/instagram","description":"Neïl Rahmouni\'s Instagram profile"},{"icon":"gitlab","url":"http://neil.rahmouni.dev/gitlab","description":"Neïl Rahmouni\'s Gitlab profile"},{"icon":"matrix","url":"http://neil.rahmouni.dev/matrix","description":"Neïl Rahmouni\'s Matrix account"}]}'));
export let rc_chadrixy = writable(JSON.parse('{"pfp":null,"main":"http://url.creative-olympics.org/chadrixy_artstation","links":[{"icon":"twitter","url":"http://url.creative-olympics.org/chadrixy_twitter","description":"Chadrixy\'s Twitter profile"}]}'));
export let rc_footer = writable(JSON.parse('{"links":[{"icon":"gitlab","url":"https://gitlab.com/neil3000/traffic-simulator","description":"Project on Gitlab"}]}'));
export let rc_feedback_email = writable('trafficsim@rahmouni.dev');
export let rc_loginProviders = writable(JSON.parse('{"Apple":"Disabled","Google":"Enabled","Microsoft":"Hidden"}'));
export let rc_banner_project_url = writable("https://gitlab.com/neil3000/traffic-simulator")