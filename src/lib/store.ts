import { writable, type Writable } from "svelte/store";
import type { Tile } from "./Tile";
import type Road from "./Road";

export const tiles: Writable<Tile[]> = writable([]);
export const roads: Writable<Road[]> = writable([]);
export const showLogo = writable(false);
export const placing: Writable<any> = writable(null);
export const selectedTiles: Writable<Tile[]> = writable([]);
export const fast = writable(false);
export const measuring = writable(0);
export const measuringLogs: Writable<number[]> = writable([]);
export const measuringLogsTmp: Writable<(number[])[]> = writable([]);